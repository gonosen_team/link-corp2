<?php
// 接続していいのはデモ環境のみ、本番環境で構築するならここをコメントアウト
if (($_SERVER['HOST_COMP_COND'] !== 'stagegroup' and strpos(__FILE__, "stagedemo") === false) or strpos(__FILE__, "demopage.jp") === false) {
    header('HTTP/1.0 404 Not Found');
    exit();
}

require_once __DIR__ . '/common/config.php';

if (DB_NAME === 'develop' or DB_NAME === 'master') {
    throw new Exception('DBの設定がsampleのままでは・・・？');
    die();
}

if (count($PDO->fetch("SHOW TABLES LIKE 'APP_ID_PASS'"))) {
    $table_exis_flg = true;
}

const IP = '210.138.173.44,114.173.123.220,180.17.25.205,118.151.96.205,180.10.246.236,180.26.4.229,61.119.173.50,110.2.115.221,14.161.0.18,118.69.226.204,113.161.70.50,118.69.35.102,115.79.192.120,118.69.34.206,183.77.121.142,118.69.218.244,150.249.196.153';

if ($table_exis_flg !== true and filter_input(INPUT_POST, 'act') === 'reg') {
    extract(utilLib::getRequestParams('post', [8, 7], true));

    $PDO->regist("START TRANSACTION");
    $PDO->regist("CREATE TABLE `APP_ID_PASS` (
        `RES_ID` int(10) UNSIGNED NOT NULL,
        `BO_ID` tinytext,
        `BO_PW` tinytext,
        `PERMIT_IP_LST` text NOT NULL,
        `UPD_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `DEL_FLG` char(1) DEFAULT '0'
    ) ");
    $PDO->regist("ALTER TABLE `APP_ID_PASS` ADD PRIMARY KEY (`RES_ID`)");
    $PDO->regist("ALTER TABLE `APP_ID_PASS` MODIFY `RES_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
    $PDO->regist("INSERT INTO `APP_ID_PASS`(`RES_ID`, `BO_ID`, `BO_PW`, `PERMIT_IP_LST`) VALUES (1, 'zeeksdg', :pass, :ip)", [['KEY' => 'pass', 'VAL' => password_hash('pass', PASSWORD_DEFAULT)], ['KEY' => 'ip', 'VAL' => IP]]);
    $PDO->regist("INSERT INTO `APP_ID_PASS`(`RES_ID`, `BO_ID`, `BO_PW`, `PERMIT_IP_LST`) VALUES (2, :bo_id, :bo_pw, 'free')", [['KEY' => 'bo_id', 'VAL' => $bo_id], ['KEY' => 'bo_pw', 'VAL' => password_hash($bo_pw, PASSWORD_DEFAULT)]]);

    if ($fvl_flg === '1') {
        $PDO->regist("INSERT INTO `APP_ID_PASS`(`RES_ID`, `BO_ID`, `BO_PW`, `PERMIT_IP_LST`) VALUES (3, 'fvl', :pass, 'free')", [['KEY' => 'pass', 'VAL' => password_hash('pass', PASSWORD_DEFAULT)]]);
    }

    if ($a1_flg === '1') {
        $PDO->regist("CREATE TABLE `MAIL_INFO` (
            `MID` int(3) NOT NULL,
            `FORM_NAME` text NOT NULL COMMENT 'フォーム名',
            `DIR_NAME` text NOT NULL COMMENT :dir_comment,
            `CONF_EMAIL` text NOT NULL COMMENT '管理者宛のメールアドレス',
            `RETURN_SUBJECT` text NOT NULL COMMENT 'リターンメールの件名',
            `RETURN_BODY` text NOT NULL COMMENT 'リターンメールの本文'
        ) ", [['KEY' => 'dir_comment', 'VAL' => '該当フォームのディレクトリ名、dirname($_SERVER["SCRIPT_NAME"])']]);

        $PDO->regist("ALTER TABLE `MAIL_INFO` ADD PRIMARY KEY (`MID`)");

        $PDO->regist("INSERT INTO `MAIL_INFO`(`MID`, `FORM_NAME`, `DIR_NAME`) VALUES (1, :form_name, :dir_name)", [['KEY' => 'form_name', 'VAL' => $form_name], ['KEY' => 'dir_name', 'VAL' => $dir_name]]);
    }

    if ($n3_flg === '1') {
        // bindはカラムにしか使えない！
        $n3_prefix = strtoupper(preg_replace('/[^0-9a-zA-Z]/', '', $n3_prefix));
        $PDO->regist("CREATE TABLE `{$n3_prefix}_CONFIG_PAGE` (
            `RES_ID` int(11) DEFAULT NULL,
            `PAGE_FLG` int(11) DEFAULT '0'
        )");
        $PDO->regist("INSERT INTO `{$n3_prefix}_CONFIG_PAGE` (`RES_ID`, `PAGE_FLG`) VALUES (1, 0)");

        $PDO->regist("CREATE TABLE `{$n3_prefix}_LST` (
            `RES_ID` varchar(25) NOT NULL,
            `TITLE` text,
            `CONTENT` text,
            `LINK` text,
            `DISP_DATE` datetime DEFAULT NULL,
            `DISPLAY_FLG` char(1) DEFAULT '1',
            `LINK_FLG` char(1) DEFAULT '1',
            `IMG_FLG` char(1) DEFAULT '1',
            `INS_DATE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            `UPD_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `DEL_FLG` char(1) DEFAULT '0'
        )");

        $PDO->regist("ALTER TABLE `{$n3_prefix}_LST` ADD PRIMARY KEY (`RES_ID`)");
    }

    $PDO->regist("COMMIT");
}

utilLib::httpHeadersPrint('UTF-8', true, true, true, true);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex,nofollow" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">
    <title>win setup</title>
</head>
<body class="bg-light">
    <div class="container">
        <?php if ($table_exis_flg === true) {?>
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="https://www.stagegroup.jp/common_img/h_logo.png">
            <h2>winの初期セットアップ</h2>
            <p class="text-danger mt-2" style="font-size: 2rem;font-weight: bold;">もうセットアップはする必要はないぞよ(´・ω・｀)？</p>
        </div>
        <?php } else if (filter_input(INPUT_POST, 'act') === 'reg') {?>
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="https://www.stagegroup.jp/common_img/h_logo.png">
            <h2>winの初期セットアップ</h2>
            <p class="text-success mt-2" style="font-size: 2rem;font-weight: bold;">できたちょ(｀･ω･´)ゞ</p>
        </div>
        <?php } else {?>
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="https://www.stagegroup.jp/common_img/h_logo.png">
            <h2>winの初期セットアップ</h2>
            <p class="lead">必要情報を入力してちょ</p>
        </div>
        <div class="row">
            <div class="col-12 offset-md-3">
                <form action="./win-setup.php" method="post" autocomplete="off">
                    <h4 class="mb-3">管理画面のアカウント</h4>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">お客さん用ID</label>
                            <input type="text" name="bo_id" class="form-control" placeholder="stagegroup" value="" required autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">お客さん用パスワード</label>
                            <input type="password" name="bo_pw" class="form-control" value="<?=makePass()?>" required autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <button type="button" id="disp_pw" class="btn btn-outline-secondary mr-2">表示</button>
                            <button type="button" id="make_pw" class="btn btn-outline-secondary">生成</button>
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox mb-1">
                        <input type="checkbox" name="fvl_flg" value="1" class="custom-control-input" id="fvl-flg">
                        <label class="custom-control-label" for="fvl-flg" style="font-weight: bold;">FVL案件の場合はチェック</label>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <hr class="mb-4">
                        </div>
                    </div>
                    <h4 class="mb-3">プログラム</h4>
                    <div class="d-block my-3">
                        <label>A1</label>
                        <div class="custom-control custom-radio">
                            <input id="a1-on-flg" name="a1_flg" value="1" type="radio" class="custom-control-input" checked required>
                            <label class="custom-control-label" for="a1-on-flg">あり</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="a1-off-flg" name="a1_flg" value="0" type="radio" class="custom-control-input" required>
                            <label class="custom-control-label" for="a1-off-flg">なし</label>
                        </div>
                    </div>
                    <div class="a1-row">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">A1のページ名</label>
                                <input type="text" name="form_name"" class="form-control" placeholder="”お問合せ”とか" value="" required autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">A1のディレクトリ名</label>
                                <input type="text" name="dir_name"" class="form-control" placeholder="”/contact”とか、TOPパスのスラッシュからね！！" value="" required autocomplete="off" pattern="^/[0-9A-Za-z/]+$">
                                <span class="small">&#8251;2つ以上ある場合はごめんちょ(´・ω・｀)</span>
                            </div>
                        </div>
                    </div>
                    <div class="d-block my-3">
                        <label>N3</label>
                        <div class="custom-control custom-radio">
                            <input id="n3-on-flg" name="n3_flg" value="1" type="radio" class="custom-control-input" checked required>
                            <label class="custom-control-label" for="n3-on-flg">あり</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="n3-off-flg" name="n3_flg" value="0" type="radio" class="custom-control-input" required>
                            <label class="custom-control-label" for="n3-off-flg">なし</label>
                        </div>
                        <span class="small">&#8251;ベーシックなテーブルしか作らないから、S7とかつく場合はなしにしたほうが楽かもよー</span>
                    </div>
                    <div class="row n3-row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">N3のディレクトリ名</label>
                            <input type="text" name="n3_prefix"" class="form-control" placeholder="”news”とか、こっちはスラッシュ不要だよ！" value="" required autocomplete="off">
                            <span class="small">&#8251;2つ以上ある場合はごめんちょ(´・ω・｀)</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <button type="submit" name="submit-btn" class="btn btn-primary btn-lg btn-block">これで作る！</button>
                        </div>
                    </div>
                    <!-- 情け程度の対策、chromeはだめ┐(´д｀)┌ﾔﾚﾔﾚ -->
                    <input type="password" name="dummypass" style="top: -100px; left: -100px; position: fixed;" />
                    <input type="hidden" name="act" value="reg">
                </form>
            </div>
        </div>
        <?php }?>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>
    $(function() {
        // input属性のものを一括で取得する
        var inputItem = document.getElementsByTagName("input");
        // 全てオートコンプリートをOFFにする
        for (var i = 0; i < inputItem.length; i++) {
            inputItem[i].autocomplete = "off";
        }

        $('#disp_pw').on('click', function() {
            let el = $(this);
            let inp_el = $('input[name="bo_pw"]');
            if (el.hasClass('pw-disp-on')) {
                el.removeClass('pw-disp-on');
                el.text('表示');
                inp_el.prop('type', 'password');
            } else {
                el.addClass('pw-disp-on');
                el.text('隠す');
                inp_el.prop('type', 'text');
            }
        });

        $('#make_pw').on('click', function() {
            const string = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            let make_p = '';
            for (let i = 0; i < 10; i++) {
                make_p += string.charAt(Math.floor(Math.random() * string.length));
            }
            $('input[name="bo_pw"]').val(make_p);
        });

        $('input[name="a1_flg"]').on('change', function() {
            if ($(this).val() !== '0') {
                $('.a1-row').show();
                $('input[name="form_name"]').prop('required', true);
                $('input[name="dir_name"]').prop('required', true);
            } else {
                $('.a1-row').hide();
                $('input[name="form_name"]').prop('required', false);
                $('input[name="dir_name"]').prop('required', false);
            }
        });

        $('input[name="n3_flg"]').on('change', function() {
            if ($(this).val() !== '0') {
                $('.n3-row').show();
                $('input[name="n3_prefix"]').prop('required', true);
            } else {
                $('.n3-row').hide();
                $('input[name="n3_prefix"]').prop('required', false);
            }
        });

    });
    </script>

</body>
</html>
