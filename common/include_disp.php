<?php
/**
 * インクルード処理用関数
 */

if (is_file(__DIR__ . '/config.php')) {
    require_once __DIR__ . '/config.php';
}

// SP階層？
$SP_FLG = strpos(__DIR__, '/sp/') !== false;

// PCのcommonにyamlファイルがあるかのチェック
if ((!$SP_FLG and is_file(__DIR__ . '/seo.yaml')) or ($SP_FLG and is_file(__DIR__ . '/../../common/seo.yaml'))) {
    require_once 'Spyc.php'; // yamlライブラリ
    $data = spyc_load_file(($SP_FLG) ? __DIR__ . '/../../common/seo.yaml' : __DIR__ . '/seo.yaml');

    if (!is_array($data)) {
        exit('yaml file error');
    }

    foreach ($data['seo'] as $v) {
        if (strpos($_SERVER['REQUEST_URI'], $v['page']) !== false) {
            $SEO = $v;
            break;
        }
    }

    // 一致するのがなかったら
    if (!isset($SEO)) {
        $SEO = $data['default'];
    }

    define('SEO_TITLE', $SEO['title']);
    define('SEO_DESCRIPTION', $SEO['description']);
    define('SEO_KEYWORDS', $SEO['keywords']);
    define('SEO_H1', $SEO['h1']);
    unset($SEO, $data);
}

/**
 * sample code
 *
 * <title><?=SEO_TITLE?></title>
 * <meta name="description" content="<?=SEO_DESCRIPTION?>">
 * <meta name="Keywords" content="<?=SEO_KEYWORDS?>">
 */

// 相対パスを取得

// このサイトのＴＯＰ階層を抽出する（裏側のファイルのパスなどに使用）
$base_path = str_replace('/common', '', dirname(__FILE__));

// ドキュメントルート ディレクトリのパスを除去する
$inc_path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $base_path) . '/';

// ディレクトリ名
$plc = basename(dirname($_SERVER['PHP_SELF']));

/**
 * 置換をすれば楽にパスが変更できる
 * href="../ → href="{$inc_path}
 * src="../ → src="{$inc_path}
 */

/**
 * ヘッダー用
 */
function DispHeader()
{
    global $inc_path;

    /**
     * variable functions
     * <h1>{$cname('SEO_H1')}</h1>
     */
    $cname = 'constant';

    $html = <<<EDIT
    <header id="header" class="header">
    <div class="container">
      <figure class="hover pull-left"><a href="{$inc_path}"><img src="{$inc_path}common_img/logo.png" alt="TLC テクノロジー・リンク株式会社"></a></figure>
      <ul class="pull-right flex flex_jus_end flex_align_item_center ul_header01">
        <li><a href="tel:045-476-2421"><img src="{$inc_path}common_img/icontell.png" alt="045-476-2421"></a></li>
        <li><a href="{$inc_path}contact/"><img src="{$inc_path}common_img/iconmail.png" alt="お問い合わせ"></a></li>
        <li><div class="hamburger"><div class="hamburger_inside"><span></span><span></span><span></span></div></div></li>
      </ul>
    </div>
    <div class="bg_sky nav">
      <nav class="container nav__inside">
        <ul class="list_menu flex">
          <li>
            <a href="{$inc_path}about/"><span>TLCについて</span>
            </a>
          </li>
          <li>
            <a href="{$inc_path}solutions/"><span>ソリューションを探す</span>
            </a>
          </li>
          <li class="hassub">
            <a href="javascript:void(0)" class="acr_title"><span>製品一覧</span></a>
            <ul class="acr_con">
              <li><a href="{$inc_path}conv/">コンバージャー</a></li>
              <li><a href="{$inc_path}conv_new/">コンバージャー派生モデル品</a></li>
              <li><a href="{$inc_path}plus/">通録プラス</a></li>
              <li><a href="{$inc_path}busylight/">Busy light for Lync</a></li>
              <li><a href="{$inc_path}dc_log/">エンタープライズ通話録音DC-LOG</a></li>
              <li><a href="{$inc_path}pbx/">通話録音PBX</a></li>
              <li><a href="{$inc_path}smart/">SmartWORKS</a></li>
            </ul>
          </li>
          <li>
            <a href="{$inc_path}case/"><span>導入事例</span>
            </a>
          </li>
          <li>
            <a href="{$inc_path}topics/"><span>トピックス</span>
            </a>
          </li>
          <li><a class="roboto en" href=""><b>ENGLISH</b></a></li>
        </ul>
      </nav>
    </div>
  </header>
EDIT;

    // 内容を返す
    return $html;
}

/**
 * サイド
 */
function DispContact()
{
    global $inc_path;

    $html = <<<EDIT
    <section class="sec_top06">
      <div class="container">
        <h2 class="tt01 tt01_center">
          <img src="{$inc_path}images/contact.png" alt="contact">
          <span>お問い合わせ</span>
        </h2>
        <div class="inline_block relative item_sec_top06">
          <h3 class="gothic text-bold f24 blue mt25">お問い合わせはお電話・メールフォームにてお気軽にご連絡ください</h3>
          <ul class="flex mt15">
            <li class="">
              <dl class="phone">
                <dt><a href="tel:045-476-2421"><span class="roboto">045-476-2421</span></a></dt>
                <dd class="f18">【受付時間】平日9:00~17:00</dd>
              </dl>
            </li>
            <li>
              <a href="{$inc_path}contact/" class="btn_contact"><span>お問い合わせ</span></a>
            </li>
          </ul>
        </div>
      </div>
    </section>
EDIT;

    // 内容を返す
    return $html;
}

function DispSlider()
{
    global $inc_path;

    $html = <<<EDIT
    <div class="bn_sec_top04 mt30 text-center">
      <h3 class="gothic text-bold">製品についてのご質問・ご相談もお気軽にお問い合わせください。</h3>
      <ul class="mt10">
        <li>
          <dl class="phone text-center">
            <dt><a href="tel:045-476-2421"><span class="roboto">045-476-2421</span></a></dt>
            <dd class="f16 mt05">【受付時間】平日9:00~17:00</dd>
          </dl>
        </li>
        <li>
          <a href="{$inc_path}contact/" class="btn_contact"><span>お問い合わせ</span></a>
        </li>
      </ul>
    </div>
EDIT;

    // 内容を返す
    return $html;
}

function DispBannar()
{
    global $inc_path;

    $html = <<<EDIT
    <section class="conv04">
      <div class="container">
        <h2 class="text-center tt_conv gothic"><span>製品一覧</span></h2>
        <ul class="list_box_bn text-center flex flex_jus_between flex_container pdside">
          <li class="box_conv01 pb25">
            <p><span>ＴＬＣオリジナルの通話録音用アダプター</span></p>
            <h3 class="mt15 gothic">コンバージャー</h3>
            <a href="{$inc_path}conv" class="asb asb_full"></a>
          </li>
          <li class="box_conv02 pb25">
            <p><span>Ring&Presenceの2-in-1</span></p>
            <h3 class="mt15 gothic">Busylight</h3>
            <a href="{$inc_path}busylight" class="asb asb_full"></a>
          </li>
          <li class="box_conv03 after pb40">
            <h3 class="gothic">その他製品一覧</h3>
            <a href="{$inc_path}products" class="asb asb_full"></a>
          </li>
          <li class="box_conv04 after pb40">
            <h3 class="gothic">コンバージャー利用例</h3>
            <a href="{$inc_path}case" class="asb asb_full"></a>
          </li>
        </ul>
      </div>
    </section>
EDIT;

    // 内容を返す
    return $html;
}

/**
 * フッター
 */
function DispFooter()
{
    global $inc_path;

    $html = <<<EDIT
    <footer>
    <div class="info_footer">
      <div class="container">
        <div class="info_footer--top">
          <figure class="hover"><a href="{$inc_path}"><img src="{$inc_path}common_img/logo_ft.png" alt="TLC テクノロジー・リンク株式会社"></a></figure>
            <p class="white letter mt20">〒222-0033　神奈川県横浜市港北区新横浜<br>2-14-2 KDX新横浜214ビル2階</p>
            <p class="link"><a href="tel:045-476-2421">TEL:045-476-2421</a> 　FAX : 045-476-2423</p>
            <p class="mt20"><a href="{$inc_path}" class="map text-underline">アクセスマップ</a></p>
        </div>
        <div class="flex navft menu_right">
          <ul>
            <li>・<a href="{$inc_path}">トップページ</a></li>
            <li>・<a href="{$inc_path}about/">TLCについて</a></li>
            <li>・<a href="{$inc_path}solutions/">ソリューションから探す</a></li>
            <li>・<a href="{$inc_path}case/">導入事例</a></li>
            <li>・<a href="{$inc_path}topics/">トピックス</a></li>
            <li>・<a href="{$inc_path}contact/">お問い合わせ</a></li>
            <li>・<a href="{$inc_path}contact/#pp">個人情報保護方針</a></li>
          </ul>
          <ul>
            <li>・<a href="{$inc_path}products/">製品一覧</a></li>
            <li>-<a href="{$inc_path}about/">TLCについて</a></li>
            <li>-<a href="{$inc_path}conv/">コンバージャー</a></li>
            <li>-<a href="{$inc_path}conv_new/">コンバージャー派生モデル品</a></li>
            <li>-<a href="{$inc_path}busylight/">Busy light for Lync/S4B</a></li>
            <li>-<a href="{$inc_path}dc_log/">エンタープライズ通話録音DC-LOG</a></li>
            <li>-<a href="{$inc_path}pbx/">通話録音PBX連携</a></li>
            <li>-<a href="{$inc_path}smart/">SmartWORKS</a></li>
            <li>-<a href="{$inc_path}new/">新商品</a></li>
          </ul>
          <ul>
            <li><a href="{$inc_path}en/">English</a></li>
            <li>・<a href="{$inc_path}en/company/">AboutTLC</a></li>
            <li>・<a href="{$inc_path}en/products/">Products introductions</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="copyright text-center f12 white">
      <div class="container">（C）2020 テクノロジー・リンク株式会社</div>
    </div>
    <span class="page_up hover"><a href="#wrapper"><img src="{$inc_path}common_img/link01.png" alt="TOP"></a></span>
    <p class="PC"><a href="/sp/../bm.php?mode=pc" target="_blank"><img src="{$inc_path}images/pclink.png" alt="PC版"></a></p>
  </footer>
EDIT;

    // 内容を返す
    return $html;

}

/**
 * 全ページに追加
 * headタグの開始の直後
 *
 * <head>
 * <?=DispAfterHeadStartTag()?>
 *
 */
function DispAfterHeadStartTag()
{
    global $inc_path;

    $html = <<<EDIT
EDIT;

    // 内容を返す
    return $html;
}

/**
 * 全ページに追加
 * headタグの閉じの直上
 *
 * <?=DispBeforeHeadEndTag()?>
 * </head>
 *
 */
function DispBeforeHeadEndTag()
{
    global $inc_path;

    $html = <<<EDIT
EDIT;

    // 内容を返す
    return $html;
}

/**
 * 全ページに追加
 * bodyタグの開始の直後
 *
 * <body>
 * <?=DispAfterBodyStartTag()?>
 *
 */
function DispAfterBodyStartTag()
{
    global $inc_path;

    $html = <<<EDIT
EDIT;

    // 内容を返す
    return $html;
}

/**
 * 全ページに追加
 * bodyタグの終了の直上
 *
 * <?=DispBeforeBodyEndTag()?>
 * </body>
 *
 */
function DispBeforeBodyEndTag()
{
    global $inc_path;

    $html = <<<EDIT
EDIT;

    // 内容を返す
    return $html;
}

/**
 * アクセス解析用
 * 埋め込みたくないページ用にDispBeforeBodyEndTag()とは切り分ける
 */
function DispAccesslog()
{
    global $inc_path, $base_path, $SP_FLG;

    // プレビュー or 7系での制作はログを取らない
    if (filter_input(INPUT_POST, 'action') or version_compare(PHP_VERSION, '7.0.0') > 0) {
        return '';
    }

    $ua = $_SERVER['HTTP_USER_AGENT'];
    if (is_file("{$base_path}/log_sp.php") and ((strpos($ua, 'Android') !== false) and (strpos($ua, 'Mobile') !== false) or (strpos($ua, 'iPhone') !== false) or (strpos($ua, 'Windows Phone') !== false))) {
        // スマートフォンからアクセスされた場合
        $link_type = 'log_sp.php';
    } elseif (is_file("{$base_path}/log_tb.php") and ((strpos($ua, 'Android') !== false) or (strpos($ua, 'iPad') !== false) or strpos($ua, 'Silk') !== false)) {
        // タブレットからアクセスされた場合
        $link_type = 'log_tb.php';
    } else {
        // デフォルトアクセス
        $link_type = 'log.php';
    }

    // sp階層の場合は1つあげる
    $log_file_path = $SP_FLG ? $inc_path . '../' : $inc_path;

    $html = <<<EDIT
<script src="https://www.google.com/jsapi?key="></script>
<script src="https://api.all-internet.jp/accesslog/access.js"></script>
<script>
    var s_obj = new _WebStateInvest();
    document.write('<img src="{$log_file_path}{$link_type}?referrer='+escape(document.referrer)+'&st_id_obj='+encodeURI(String(s_obj._st_id_obj))+'" width="1" height="1" style="display:none">');
</script>
EDIT;

    // 内容を返す
    return $html;
}
