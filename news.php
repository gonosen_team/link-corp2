<?php
// 設定ファイルの読み込み
require_once __DIR__ . '/common/include_disp.php';
require_once __DIR__ . '/common/config_topics.php';

$sql = "
SELECT
    `RES_ID`,
    `TITLE`,
    `DISP_DATE`,
    `LINK`,
    `LINK_FLG`
FROM
    `" . PRODUCT_LST . "`
WHERE
    `DISPLAY_FLG` = '1'
AND
    `DEL_FLG` = '0'
ORDER BY
    `DISP_DATE` DESC
LIMIT
    0 , " . DBMAX_CNT . "
";

$news_contents = [];

foreach ($PDO->fetch($sql) as $i => $v) {
    // reset
    $tmp = [];

    // ID
    $tmp['id'] = $v['RES_ID'];

    // 日付
    $tmp['date'] = (new Datetime($v['DISP_DATE']))->format(DATE_FORMAT);

    // タイトル
    $tmp['title'] = $v['TITLE'];

    // リンク先
    $tmp['link'] = $v['LINK'];

    // リンク先表示タイプ
    $tmp['link_flg'] = $v['LINK_FLG'];

    // 表示されるページを算出
    $tmp['p'] = ceil(($i + 1) / SETTING_PAGE);

    // set
    $news_contents[] = $tmp;
}
unset($tmp);
