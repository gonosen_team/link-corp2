// resize
// ==================================================

$(function(){
	var timer = false;
	$(window).on('load resize', function(){
		if (timer !== false) {
			clearTimeout(timer);
		}
		timer = setTimeout(function() {
			setViewport(); // viewport
		},200);
	});
});

// setViewport
function setViewport() {
	var w = $(window).width();
	var x = 767;
	var viewport = $('meta[name=viewport]');
	if ( w <= x ) {
		viewport.attr('content', 'width=device-width');
	} else {
		viewport.attr('content', 'width=1200');
	}
}



$(document).ready(function() {
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		if (target.length) {
			$('html,body').animate({
			scrollTop: target.offset().top
			}, 1000);
			return false;
		}
		}
	});
	$(".page_up").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('.page_up').fadeIn();
            } else {
                $('.page_up').fadeOut();
            }
        });
    });
});

$(".hamburger").click(function (a) {
	a.stopPropagation()
	$(this).toggleClass("active");
	$(".bg_sky").toggleClass("show")
	$("body").toggleClass("menu-open");
	$("body").hasClass("menu-open") ?
		($("body").addClass("menu_fixed"), $(document).height(), $("body.menu-open").css({
			height: "100%"
		})) : ($("body").removeClass("menu_fixed"), $("body").css({
			height: "auto"
		}))
});

$(".acr_title").on("click", function (a) {
	a.preventDefault();
	a = $(this);
	var b = a.next(".acr_con");
	$(".acr_title").not($(this)).removeClass("open");
	$(".acr_con").not($(this).next()).slideUp("fast");
	a.toggleClass("open");
	b.slideToggle(250)
})